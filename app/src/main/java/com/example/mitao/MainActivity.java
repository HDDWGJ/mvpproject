package com.example.mitao;


import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.NavGraph;
import androidx.navigation.NavGraphNavigator;
import androidx.navigation.NavigatorProvider;
import androidx.navigation.fragment.FragmentNavigator;
import androidx.navigation.fragment.NavHostFragment;

import com.example.mitao.base.BaseActivity;
import com.example.mitao.ui.find.FindFragment;
import com.example.mitao.ui.home.HomeFragment;
import com.example.mitao.ui.me.MeFragment;
import com.example.mitao.ui.message.MessageFragment;
import com.example.mitao.ui.presenter.NullPresenter;
import com.example.mitao.utils.navigator.FixFragmentNavigator;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomnavigation.LabelVisibilityMode;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class MainActivity extends BaseActivity<NullPresenter> {
    @BindView(R.id.nav_view)
    public  BottomNavigationView navView;

    private FragmentManager mSupportFragmentManager;
    private FragmentTransaction mTransaction;
    private List<Fragment> mFragmets = new ArrayList<>();


    public HomeFragment mHomeFragment;
    private FindFragment mFindFragment;
    private MeFragment mMeFragment;
    private MessageFragment mMessageFragment;

    @Override
    public int initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        return R.layout.activity_main;
    }

    @Override
    protected void initData() {

        navView.setLabelVisibilityMode( LabelVisibilityMode.LABEL_VISIBILITY_LABELED);
        navView.setItemIconTintList(null);


        Fragment fragmentById = getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment_activity_main);
        //fragment的重复加载问题和NavController有关
        final NavController navController = NavHostFragment.findNavController(fragmentById);

        NavigatorProvider provider = navController.getNavigatorProvider();
        //设置自定义的navigator
        FixFragmentNavigator fixFragmentNavictor = new FixFragmentNavigator(this, fragmentById.getChildFragmentManager(), fragmentById.getId());
        provider.addNavigator(fixFragmentNavictor);

        NavGraph navDestinations = initNavGraph(provider, fixFragmentNavictor);
        navController.setGraph(navDestinations);

        //正确的关联方式o(￣▽￣)ｄ
        navView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull @NotNull MenuItem item) {
                navController.navigate(item.getItemId());
                return true;
            }
        });

    }



    private NavGraph initNavGraph(NavigatorProvider provider, FixFragmentNavigator fragmentNavigator) {
        NavGraph navGraph = new NavGraph(new NavGraphNavigator(provider));

        //用自定义的导航器来创建目的地
        FragmentNavigator.Destination destination1 = fragmentNavigator.createDestination();
        destination1.setId(R.id.navigation_home);
        destination1.setClassName(HomeFragment.class.getCanonicalName());
        navGraph.addDestination(destination1);


        FragmentNavigator.Destination destination2 = fragmentNavigator.createDestination();
        destination2.setId(R.id.navigation_find);
        destination2.setClassName(FindFragment.class.getCanonicalName());
        navGraph.addDestination(destination2);

        FragmentNavigator.Destination destination3 = fragmentNavigator.createDestination();
        destination3.setId(R.id.navigation_message);
        destination3.setClassName(MessageFragment.class.getCanonicalName());
        navGraph.addDestination(destination3);

        FragmentNavigator.Destination destination4 = fragmentNavigator.createDestination();
        destination4.setId(R.id.navigation_me);
        destination4.setClassName(MeFragment.class.getCanonicalName());
        navGraph.addDestination(destination4);


        navGraph.setStartDestination(R.id.navigation_home);

        return navGraph;
    }


    @Override
    public void onBackPressed() {
        finish();
    }

}