package com.example.mitao;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.TextView;

import com.example.mitao.base.BaseActivity;
import com.example.mitao.ui.presenter.NullPresenter;
import com.example.mitao.utils.MemoryTools;
import com.example.mitao.utils.SPfUtil;
import com.example.mitao.utils.key.SPFKey;
import com.example.mitao.utils.status.StatusCompat;

import butterknife.BindView;

public class SplashActivity extends BaseActivity<NullPresenter> {
    private CountDownTimer mTimer;

    @BindView(R.id.clear_text)
    TextView btn_back;
    private Boolean isClick = false;


    @Override
    public int initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_Launcher);
        return R.layout.activity_splash;
    }

    @Override
    protected void initData() {
        StatusCompat.setStatusBarColor(this,   Color.TRANSPARENT);
        initView();
    }

    //初始化View
    private void initView() {

        if (isFirst()) {
            startActivity(new Intent(SplashActivity.this, MainActivity.class));
            this.finish();
        } else {

            if (mTimer == null) {
                long waitTime = 5000;
                mTimer = new CountDownTimer(waitTime, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        if (!SplashActivity.this.isFinishing()) {
                            int remainTime = (int) (millisUntilFinished / 1000L);

                            //如果是低端机,到1秒的时候,要显示 "跳转中"
                            if (MemoryTools.isLowLevelMachine(SplashActivity.this)) {
                                if (remainTime >= 1) {
                                    btn_back.setText(remainTime + "秒关闭");
                                }
                                if (remainTime == 0) {
                                    btn_back.setText("跳转中...");
                                }
                            } else {
                                btn_back.setText(remainTime + "秒关闭");
                                //高端机,到1秒的时候,直接跳转
                                if (remainTime == 1) {
                                        startMainActivity();
                                }
                            }
                        }
                    }

                    @Override
                    public void onFinish() {
                        //如果是低端机,到1秒的时候,要显示 "跳转中"
                        if (MemoryTools.isLowLevelMachine(SplashActivity.this)) {
                                startMainActivity();
                        }
                    }
                };
                mTimer.start();
            }

            btn_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startMainActivity();
                }
            });
        }
    }

    private void startMainActivity() {
        if (!isClick) {
            isClick = true;
            startActivity(new Intent(SplashActivity.this, MainActivity.class));
            this.finish();
        }
    }


    //判断程序是否第一次运行
    private boolean isFirst() {
        boolean isFirst = SPfUtil.getInstance().getBoolean(SPFKey.SHARE_IS_FIRST);
        if (!isFirst) {
            SPfUtil.getInstance().setBoolean(SPFKey.SHARE_IS_FIRST, true);
            //是第一次运行
            return true;
        } else {
            return false;
        }

    }
}