package com.example.mitao.ui.home.activitytest;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.example.mitao.R;
import com.example.mitao.base.BaseActivity;
import com.example.mitao.ui.presenter.NullPresenter;
import com.example.mitao.utils.ImageLoadUtils;

import butterknife.BindView;

public class ImagActivity extends BaseActivity<NullPresenter> {


    @BindView(R.id.imgage1)
    ImageView imgage1;
    @BindView(R.id.imgage2)
    ImageView imgage2;
    @BindView(R.id.imgage3)
    ImageView imgage3;

    public static void startUI(Context context){
        Intent intent = new Intent(context, ImagActivity.class);
//        intent.putExtra(KEY_TITLE, title);
//        intent.putExtra(KEY_URL, url);
        context.startActivity(intent);
    }


    @Override
    public int initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        return R.layout.activity_imag;
    }

    @Override
    protected void initData() {

        String imgpath = "http://img.jiuzheng.com/memberlogo/s/57/0a/570af0f48f1e0327178b468d.jpg";
        ImageLoadUtils.ImageLoad(this,imgpath,imgage1);

        ImageLoadUtils.ImageLoad(this,imgpath,imgage2,30);
        ImageLoadUtils.ImageLoad(this,imgpath,imgage3,80);
    }
}