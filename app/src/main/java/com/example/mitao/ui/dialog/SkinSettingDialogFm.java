package com.example.mitao.ui.dialog;

import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

;
;

import com.example.mitao.R;
import com.example.mitao.base.BaseDialogFragment;


/**
 * Created by kenny on 2020/5/23.
 * 换肤
 */
public class SkinSettingDialogFm extends BaseDialogFragment {

    private static final String TAG = "SkinSettingDialogFm";
    private RadioButton mRijian,mYejian;
    private skinsettingBack mBack;
    private TextView close_tv;
    public SkinSettingDialogFm(skinsettingBack back){
        this.mBack = back;
    }
    @Override
    public int getContentViewResId() {
        return R.layout.dialog_skin_setting;
    }

    @Override
    public int getCloseBtnId() {
        return R.id.close_tv;
    }

    @Override
    public String getLocation() {
        return BOTTM;
    }


    @Override
    public View initData(View view) {
        mRijian = view.findViewById(R.id.rijian);
        mYejian = view.findViewById(R.id.yejian);
        close_tv=view.findViewById(R.id.close_tv);
        close_tv.setText("取消");
//        Boolean whiteAndHeave = SPfUtil.getInstance().getBoolean(SPFKey.WhiteAndheaven);
//        if(whiteAndHeave){
//            mYejian.setChecked(true);
//        }else {
//            mRijian.setChecked(true);
//        }

        mRijian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             try {

//                SPfUtil.getInstance().setBoolean(SPFKey.WhiteAndheaven,false);
                dismiss();
                mBack.settingBack(false);
//                EventBus.getDefault().post(new RefreshUiBean());
            }catch (Exception e){
                 e.printStackTrace();
            }
            }
        });
        mYejian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

//                    SPfUtil.getInstance().setBoolean(SPFKey.WhiteAndheaven,true);
                    dismiss();
                    mBack.settingBack(true);
//                    EventBus.getDefault().post(new RefreshUiBean());
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });


        mRijian.setText("日间模式");
        mYejian.setText("夜间模式");


        return view;
    }

  public interface  skinsettingBack{
        void settingBack(Boolean bool);
  }
}

