package com.example.mitao.ui.presenter;


import com.example.mitao.model.CourseRetrofit;
import com.example.mitao.mvpbase.presenter.BasePresenter;
import com.example.mitao.net.ResponseResult;
import com.example.mitao.net.interceptor.DefaultObserver;
import com.example.mitao.ui.home.activitytest.bean.TestBean;
import com.example.mitao.ui.home.activitytest.contract.TestBackContract;

import java.util.List;


public class NullPresenter extends BasePresenter<TestBackContract.View> {
    @Override
    public void onStart() {
    }

}
