package com.example.mitao.ui.release;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.mitao.R;
import com.example.mitao.base.BaseActivity;
import com.example.mitao.ui.home.activitytest.TestActivity;
import com.example.mitao.ui.home.activitytest.contract.TestBackContract;
import com.example.mitao.ui.home.activitytest.presenter.TestPresenter;

import butterknife.BindView;

public class ReleaseActivity extends BaseActivity<TestPresenter> implements TestBackContract.View {

    @BindView(R.id.right_text)
    public TextView right_text;
    @BindView(R.id.contentEt)
    public EditText contentEt;
    @BindView(R.id.textNumTv)
    public TextView textNumTv;


    public static void startUI(Context context){
        Intent intent = new Intent(context, ReleaseActivity.class);
//        intent.putExtra(KEY_TITLE, title);
//        intent.putExtra(KEY_URL, url);
        context.startActivity(intent);
    }
    @Override
    public int initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        return R.layout.activity_release;
    }

    @Override
    protected void initData() {
        right_text.setText("发布");
        right_text.setVisibility(View.VISIBLE);
        contentEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {


                return false;
            }
        });
        contentEt.addTextChangedListener(new TextWatcher() {
            private int editStart ;
            private int editEnd ;
            private CharSequence temp;
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                temp = charSequence;
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                textNumTv.setText(String.format("%d/500", editable.length()));
                editStart = contentEt.getSelectionStart();
                editEnd = contentEt.getSelectionEnd();
                if(temp.length()>500){
                    showToast("字数不能超过500");
                    editable.delete(editStart-1, editEnd);
                    int tempSelection = editStart;
                    contentEt.setText(editable);
                    contentEt.setSelection(tempSelection);
                }
//                mTextNumTv.setText((500 - editable.length()) / 500);
            }
        });

    }

    @Override
    public void showSuccess(int rat, String message, Object data) {

    }

    @Override
    public void onFailt(int code, String message) {

    }
}