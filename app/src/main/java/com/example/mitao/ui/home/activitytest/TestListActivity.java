package com.example.mitao.ui.home.activitytest;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.mitao.R;
import com.example.mitao.base.BaseActivity;
import com.example.mitao.ui.home.activitytest.adatper.WorksViewTypeAdapter;
import com.example.mitao.ui.home.activitytest.bean.TestBean;
import com.example.mitao.ui.home.activitytest.presenter.TestPresenter;
import com.example.mitao.ui.presenter.NullPresenter;
import com.example.mitao.view.NetDataView;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class TestListActivity  extends BaseActivity<NullPresenter> implements NetDataView.NotNetOnClickListener {

    private List<TestBean> list = new ArrayList();
    @BindView(R.id.notView)
    NetDataView notView;
    @BindView(R.id.recycler)
    RecyclerView recycler;

    @BindView(R.id.textTitle)
    public TextView textTitle;
    @BindView(R.id.refreshLayout)
    RefreshLayout refreshLayout;

    private WorksViewTypeAdapter mAdapter;

    public static void startUI(Context context){
        Intent intent = new Intent(context, TestListActivity.class);
//        intent.putExtra(KEY_TITLE, title);
//        intent.putExtra(KEY_URL, url);
        context.startActivity(intent);
    }

    @Override
    public int initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        return R.layout.activity_test_list;
    }


    @Override
    protected void initData() {
        textTitle.setText("列表数据加载");

        for (int i =0;i<20;i++){
            TestBean bean =   new TestBean();
            bean.setCode(i+"");
            bean.setName(i+"名字");
            list.add(bean);
        }

        mAdapter = new WorksViewTypeAdapter(list,context);

        recycler.setLayoutManager(new LinearLayoutManager(this));
        recycler.setAdapter(mAdapter);

        refreshLayout.setRefreshHeader(new ClassicsHeader(this));
        refreshLayout.setRefreshFooter(new ClassicsFooter(this));
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull @NotNull RefreshLayout refreshLayout) {
                refreshLayout.finishRefresh(2000);
            }
        });
        refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull @NotNull RefreshLayout refreshLayout) {
                refreshLayout.finishLoadMore(2000);
            }
        });

        notView.bindingParentViewAndDate(recycler,list,this);
    }

    @Override
    public void operation(View view) {

    }
}