package com.example.mitao.ui.home.activitytest.contract;


import com.example.mitao.mvpbase.BaseView;

/**
 * 请求 成功后返回
 */
public interface TestBackContract {

    interface View extends BaseView {
        void showSuccess(int rat, String message, Object data);//成功
        void onFailt(int code, String message); //失败

    }


}
