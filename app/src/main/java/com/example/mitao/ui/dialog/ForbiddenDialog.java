package com.example.mitao.ui.dialog;


import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.mitao.R;
import com.example.mitao.base.BaseDialogView;
import com.example.mitao.utils.Utils;


/**
 * 禁用户发言、
 */
public class ForbiddenDialog extends BaseDialogView {


    private Builder builder;

    private TextView close_iv,determine;


    public ForbiddenDialog(Context context) {
        super(context);
        this.setCancelable(false);
    }

    public ForbiddenDialog(Context context,Builder builders) {
        super(context);
        this.builder = builders;
    }

    public String getForecastChat(){
        String[] doc = {"本场比赛我看好%s!", "支持%s!"};
        int index = (int) (Math.random() * doc.length);
        return doc[index];
    }



    @Override
    public int getContentViewResId() {
        return R.layout.dialog_forbidden;
    }

    @Override
    public View initData(View view) {

      TextView name =  view.findViewById(R.id.NameTv);
      if(builder.name !=null){
          name.setText(builder.name);
      }


//      ImageView img = view.findViewById(R.id.hand_Img);
//      ImageLoaderHelper.loadImageByGlide(img,builder.images,R.mipmap.wode_img_touxiang_weidenglu,null);

      TextView titleTv = view.findViewById(R.id.titleTv);
      titleTv.setText("确定将该用户禁言？");

        close_iv = view.findViewById(R.id.close_iv);
        close_iv.setText("取消");
        close_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setVisibility(GONE);
            }
        });
        determine = view.findViewById(R.id.determine);
        determine.setText(  "确定");
        determine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setVisibility(GONE);
            }
        });

        return view;
    }

    public void resize() {
        FrameLayout.LayoutParams flp = new FrameLayout.LayoutParams(Utils.getWindowWidth(getContext()) - Utils.dipToPx(getContext(), 5F), ViewGroup.LayoutParams.WRAP_CONTENT);
        getView().setLayoutParams(flp);
    }


    @Override
    public int getCloseBtnId() {
        return 0;
    }

    @Override
    public String getLocation() {
        return CENTER;
    }



    public interface ForecastSuccessListener {
        void cancel(String img);

        void accessSuccessful(int myForecast);// 发布成功

        void error(String error);// 发布失败
    }
    public static class Builder {

        private int id;//主队id
        private String name;
        private String images;
        private int room_id;
        private int user_id;
        private ForecastSuccessListener mListener;
        private Context mContext;

        public Builder roomId(int room_id) {
            this.room_id = room_id;
            return this;
        }
        public Builder userId(int user_id) {
            this.user_id = user_id;
            return this;
        }
        public Builder mainTeamId(int id) {
            this.id = id;
            return this;
        }
        public Builder setName(String name) {
            this.name = name;
            return this;
        }
        public  Builder setImages(String img) {
            this.images = img;
            return this;
        }

        public Builder setListener(ForecastSuccessListener listener) {
            this.mListener = listener;
            return this;
        }


        public Builder(Context context){
            this.mContext = context;
        }

        public ForbiddenDialog build(){
            return new ForbiddenDialog(mContext,this);
        }

    }

}
