package com.example.mitao.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.mitao.R;
import com.example.mitao.base.BaseFragment;
import com.example.mitao.databinding.FragmentHomeBinding;
import com.example.mitao.ui.home.activitytest.TestActivity;
import com.example.mitao.ui.home.activitytest.presenter.HomePresenter;
import com.example.mitao.ui.home.activitytest.presenter.TestPresenter;
import com.example.mitao.ui.presenter.NullPresenter;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;
import kotlin.jvm.JvmField;

public class HomeFragment extends BaseFragment<NullPresenter> {


    @BindView(R.id.text_home)
    public TextView mTextHome;

    @BindView(R.id.button)
    public Button mButton;

    @Override
    public int getContentViewResId() {
        return R.layout.fragment_home;
    }

    @Override
    public View initView(@NotNull View contentView, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TestActivity.startUI(getContext());
            }
        });
        return contentView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        binding = null;
    }
}