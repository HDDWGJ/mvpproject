package com.example.mitao.ui.home.activitytest.adatper;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mitao.R;
import com.example.mitao.ui.home.activitytest.bean.TestBean;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Created by kenny on 2020/5/18.
 */
public class WorksViewTypeAdapter extends RecyclerView.Adapter<WorksViewTypeAdapter.myViewView> {

    private List<TestBean> mList;
    private Context mContext;
    public WorksViewTypeAdapter(List<TestBean> list, Context context){
        mList = list;
        mContext = context;
    }
    @NonNull
    @NotNull
    @Override
    public myViewView onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.item_expert_head,parent,false);
        return new myViewView(v);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull myViewView holder, int position) {
        TestBean bean = mList.get(position);
        holder.TextView.setText(bean.getName());
//        holder.headIv
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class  myViewView extends RecyclerView.ViewHolder{
        private ImageView headIv;
        private TextView TextView;
        public myViewView(@NonNull @NotNull View itemView) {
            super(itemView);
            headIv = itemView.findViewById(R.id.headIv);
            TextView = itemView.findViewById(R.id.expertNameTv);
        }
    }
}

