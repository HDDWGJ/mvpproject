package com.example.mitao.ui.me;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.mitao.R;
import com.example.mitao.base.BaseFragment;
import com.example.mitao.databinding.FragmentMeBinding;
import com.example.mitao.ui.presenter.NullPresenter;
import com.example.mitao.ui.release.ReleaseActivity;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;

public class MeFragment extends BaseFragment<NullPresenter> implements View.OnClickListener{

    @BindView(R.id.release_but)
    Button release_but;
    @BindView(R.id.setUp)
    ImageView setUp;
    @BindView(R.id.release)
    ImageView  mRelease;

    @Override
    public int getContentViewResId() {
        return R.layout.fragment_me;
    }

    @Override
    public View initView(@NotNull View contentView, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        setDataView();
        return contentView;
    }

    private void setDataView(){
        release_but.setOnClickListener(this);
        setUp.setOnClickListener(this);
        mRelease.setOnClickListener(this);
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.release:
            case R.id.release_but:
                ReleaseActivity.startUI(getContext());
                break;
        }
    }
}