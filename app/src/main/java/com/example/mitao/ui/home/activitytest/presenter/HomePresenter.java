package com.example.mitao.ui.home.activitytest.presenter;


import com.example.mitao.model.CourseRetrofit;
import com.example.mitao.mvpbase.presenter.BasePresenter;
import com.example.mitao.net.ResponseResult;
import com.example.mitao.net.interceptor.DefaultObserver;
import com.example.mitao.ui.home.activitytest.bean.TestBean;
import com.example.mitao.ui.home.activitytest.contract.TestBackContract;

import java.util.List;


public class HomePresenter extends BasePresenter<TestBackContract.View> {
    @Override
    public void onStart() {
    }

     /**
      *获取数据
      */
     public void getTest(){
         CourseRetrofit.getCourse(new DefaultObserver<List<TestBean>>() {
             @Override
             public void onSuccess(ResponseResult<List<TestBean>> result) {
                 if(isViewActive()){
//                     getView().refishData(result.getData(),tag);
//                     getView().hideProgress();
                 }
             }

             @Override
             public void onException(int code,String eMsg) {
                 if(isViewActive()){
                     getView().onFailt(code,eMsg);

                 }
             }
         },"");

     }
}
