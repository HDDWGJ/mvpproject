package com.example.mitao.ui.home.activitytest;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.mitao.R;
import com.example.mitao.base.BaseActivity;
import com.example.mitao.ui.dialog.ForbiddenDialog;
import com.example.mitao.ui.dialog.SkinSettingDialogFm;
import com.example.mitao.ui.home.activitytest.contract.TestBackContract;
import com.example.mitao.ui.home.activitytest.presenter.TestPresenter;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;
import constant.UiType;
import kotlin.jvm.JvmField;
import listener.Md5CheckResultListener;
import listener.UpdateDownloadListener;
import model.UiConfig;
import model.UpdateConfig;
import update.UpdateAppUtils;


public class TestActivity extends BaseActivity<TestPresenter> implements TestBackContract.View {

    public static final String KEY_TITLE = "KEY_TITLE";
    public static final String KEY_URL = "KEY_URL";


    @BindView(R.id.text)
    public TextView mText;
    @BindView(R.id.toast_tv)
    public TextView toast_tv;
    @BindView(R.id.textTitle)
    public TextView textTitle;
    @BindView(R.id.but_upload)
    public Button but_upload;
    @BindView(R.id.but_list)
    Button but_list;

    @BindView(R.id.but_img)
    Button but_img;

    @BindView(R.id.but_up)
    Button but_up;

    public static void startUI(Context context){
        Intent intent = new Intent(context, TestActivity.class);
//        intent.putExtra(KEY_TITLE, title);
//        intent.putExtra(KEY_URL, url);
        context.startActivity(intent);
    }

    public static void startUI(Context context, String title, String url){
        Intent intent = new Intent(context, TestActivity.class);
        intent.putExtra(KEY_TITLE, title);
        intent.putExtra(KEY_URL, url);
        context.startActivity(intent);
    }


    @Override
    public int initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        return R.layout.activity_test;
    }

    @Override
    protected void initData() {
        mText.setText("111111");

        getPresenter().getTest();//网络请求

        toast_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showToast("这里提示用户");
            }
        });
        but_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImagActivity.startUI(context);
            }
        });

        textTitle.setText("测试页面标题");

        but_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//               new  ForbiddenDialog
//                       .Builder(getBaseContext())
//                       .setName("传递name")
//                       .build()
//                       .show(TestActivity.this);

                SkinSettingDialogFm dialgo = new SkinSettingDialogFm(null);
                dialgo.show(getSupportFragmentManager(),"");
            }
        });

        but_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TestListActivity.startUI(context);
            }
        });

        but_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                upLoad();
            }
        });
    }


    private void upLoad(){
        UpdateConfig updateConfig = new UpdateConfig();
        updateConfig.setCheckWifi(true);
        updateConfig.setNeedCheckMd5(true);
        updateConfig.setNotifyImgRes(R.mipmap.icon_luncha);

        UiConfig uiConfig = new UiConfig();
        uiConfig.setUiType(UiType.SIMPLE);

        UpdateAppUtils
                .getInstance()
                .apkUrl("https://www.baidu.com/")
                .updateTitle("我是标题")
                .updateContent("更新了哪些内容")
                .uiConfig(uiConfig)
                .updateConfig(updateConfig)
                .setMd5CheckResultListener(new Md5CheckResultListener() {
                    @Override
                    public void onResult(boolean result) {

                    }
                })
                .setUpdateDownloadListener(new UpdateDownloadListener() {
                    @Override
                    public void onStart() {

                    }

                    @Override
                    public void onDownload(int progress) {

                    }

                    @Override
                    public void onFinish() {

                    }

                    @Override
                    public void onError(@NotNull Throwable e) {

                    }
                })
                .update();
    }
    @Override
    public void showSuccess(int rat, String message, Object data) {

    }

    @Override
    public void onFailt(int code, String message) {

    }
}