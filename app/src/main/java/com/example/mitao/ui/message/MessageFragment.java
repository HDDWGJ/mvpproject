package com.example.mitao.ui.message;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.mitao.R;
import com.example.mitao.base.BaseFragment;
import com.example.mitao.databinding.FragmentMessageBinding;
import com.example.mitao.ui.presenter.NullPresenter;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;

public class MessageFragment extends BaseFragment<NullPresenter> {

    @BindView(R.id.text_message)
    TextView text_message;
    @Override
    public int getContentViewResId() {
        return R.layout.fragment_message;
    }

    @Override
    public View initView(@NotNull View contentView, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        setDataView();
        return contentView;
    }

    private void setDataView(){
        text_message.setText("我是消息页面");
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}