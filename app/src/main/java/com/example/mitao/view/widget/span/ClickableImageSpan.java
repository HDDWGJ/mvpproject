package com.example.mitao.view.widget.span;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.text.style.ImageSpan;
import android.view.View;

public abstract class ClickableImageSpan extends ImageSpan {
    public ClickableImageSpan(Drawable b) {
        super(b);
    }

    public ClickableImageSpan(Context ctx, Bitmap b) {
        super(ctx,b);
    }

    public abstract void onClick(View view);
}
