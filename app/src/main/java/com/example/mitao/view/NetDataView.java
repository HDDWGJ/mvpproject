package com.example.mitao.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;


import com.example.mitao.R;

import java.util.List;


/**
 * Created by Administrator on 2019/4/24.
 */

public class NetDataView extends LinearLayout {

    private Context context;


    private View parentView;
    TextView noDataTextView;

    Button btnReloadData;

    private List<Object> lists;


    public NetDataView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs, 0);
    }

    public NetDataView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public NetDataView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context, attrs, defStyleAttr);
    }


    private void initView(Context context, AttributeSet attrs, int defStyle) {
        this.context = context;
        View inflate = inflate(context, R.layout.net_data_view, this);
        noDataTextView=inflate.findViewById(R.id.no_data_textView);

        btnReloadData=inflate.findViewById(R.id.btnReloadData);

//        TypedArray mTypedArray = context.obtainStyledAttributes(attrs, R.styleable.noData);
//        //获取自定义属性和默认值
//        String noData = mTypedArray.getString(R.styleable.noData_noText);
//        if (noData!=null&&noData.length()>0){
//            noDataTextView.setText(noData);
//        }
//        mTypedArray.recycle();
    }



    public Button getButtonReloadData() {
        return btnReloadData;
    }

    public void showNoDataView(Boolean boole) {

        parentViewTodo(boole);

    }


    private void parentViewTodo(Boolean boole) {
        if (parentView == null) {
            return;
        }

        if (boole) {
            parentView.setVisibility(VISIBLE);
        } else {
            parentView.setVisibility(GONE);
        }

    }

    public void showNodataText(String data){
        noDataTextView.setText(data);
    }

    public void bindingParentView(View _parentView) {
        parentView = _parentView;
    }

    public void bindingParentViewAndDate(View _parentView,List lists,NotNetOnClickListener notNetOnClickListener) {
        parentView = _parentView;
        this.lists=lists;
        showNoDataView(true);
        btnReloadData.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(notNetOnClickListener!=null){
                    notNetOnClickListener.operation(v);
                }
            }
        });
    }

    public void notifyDataSetChanged(){
        if(lists==null){
            showNoDataView(false);
            return;
        }
        if(lists.size()>0){
            showNoDataView(true);
        }else{
            showNoDataView(false);
        }
    }

    public interface NotNetOnClickListener{
        void operation(View view);
    }

    public  boolean isConnected() {
        NetworkInfo info = getActiveNetworkInfo();
        return info != null && info.isConnected();
    }
    private  NetworkInfo getActiveNetworkInfo() {
        ConnectivityManager cm = (ConnectivityManager)this.getContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }


}
