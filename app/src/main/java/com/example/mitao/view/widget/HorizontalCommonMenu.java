//package com.example.mitao.view.widget;
//
//import android.content.Context;
//import android.content.res.TypedArray;
//import android.os.Build;
//import android.text.Spanned;
//import android.util.AttributeSet;
//import android.util.TypedValue;
//import android.view.View;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import androidx.annotation.Nullable;
//import androidx.annotation.RequiresApi;
//
//
//import com.sowell.mvpbase.mvputils.ViewUtil;
//
//import com.sowell.mvpbase.view.widget.span.TextSpanCreater;
//
//
///**
// * Created by Administrator on 2019/4/24.
// */
//
//public class HorizontalCommonMenu extends LinearLayout {
//
//    RelativeLayout itemParent;
//    ImageView menuLogoIv;
//    TextView menuTv;
//    TextView menuTv2;
//    TextView msgTv;
//    ImageView jtIv;
//    LinearLayout jtLL;
//    ImageView head;
//
//    private Context context;
//    private String menuText="";
//    private String menuText2="";
//
//    private String rightText="";
//
//    private Boolean isrightImg;
//
//
//    public HorizontalCommonMenu(Context context) {
//        this(context,null);
//    }
//
//    public HorizontalCommonMenu(Context context, @Nullable AttributeSet attrs) {
//        this(context, attrs,0);
//    }
//
//    public HorizontalCommonMenu(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
//        this(context, attrs, defStyleAttr,0);
//    }
//
//    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
//    public HorizontalCommonMenu(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
//        super(context, attrs, defStyleAttr, defStyleRes);
//        initView(context, attrs, defStyleAttr);
//    }
//
//
//    public TextView getMenuTv() {
//        return menuTv;
//    }
//
//    public TextView getMenuTv2(){
//        return menuTv2;
//    }
//
//    public TextView getMsgTv() {
//        return msgTv;
//    }
//
//    private void initView(Context context, AttributeSet attrs, int defStyle) {
//        this.context = context;
//        View inflate = inflate(context, R.layout.horizontalmenu, this);
//
//        itemParent=inflate.findViewById(R.id.itemParent);
//        menuLogoIv=inflate.findViewById(R.id.menuLogoIv);
//        menuTv=inflate.findViewById(R.id.menuTv);
//        menuTv2=inflate.findViewById(R.id.menuTv2);
//        msgTv=inflate.findViewById(R.id.msgTv);
//        jtIv=inflate.findViewById(R.id.jtIv);
//        jtLL=inflate.findViewById(R.id.jtLL);
//        head=inflate.findViewById(R.id.head);
//
//
//        final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.HorizontalCommonMenu, defStyle, 0);
//        isrightImg = a.getBoolean(R.styleable.HorizontalCommonMenu_isrightImg,true);//是否显示右侧箭头
//        menuText = a.getString(R.styleable.HorizontalCommonMenu_menuText);
//        menuText2= a.getString(R.styleable.HorizontalCommonMenu_menuText2);
//        rightText = a.getString(R.styleable.HorizontalCommonMenu_rightText);
//        int rightheadImg =  a.getResourceId(R.styleable.HorizontalCommonMenu_rightheadImg, -1);//添加右侧头像，默认不显示
//        int leftImg = a.getResourceId(R.styleable.HorizontalCommonMenu_leftImg, -1);
//        int rightImg = a.getResourceId(R.styleable.HorizontalCommonMenu_rightImg, -1);
//        int rightTextSize = a.getInteger(R.styleable.HorizontalCommonMenu_rightTextSize, -1);
//        int menuTextTextSize = a.getInteger(R.styleable.HorizontalCommonMenu_menuTextTextSize, -1);
//        int menuTextTextSize2 = a.getInteger(R.styleable.HorizontalCommonMenu_menuTextTextSize2, -1);
//
//        if (rightTextSize != -1) {
//            msgTv.setTextSize(TypedValue.COMPLEX_UNIT_PX, Utils.dipToPx(BaseManager.getInstance().getContext(), rightTextSize));
//        }
//
//        if (menuTextTextSize != -1) {
//            menuTv.setTextSize(TypedValue.COMPLEX_UNIT_PX, Utils.dipToPx(BaseManager.getInstance().getContext(), menuTextTextSize));
//        }
//
//        if (menuTextTextSize2 != -1) {
//            menuTv2.setTextSize(TypedValue.COMPLEX_UNIT_PX, Utils.dipToPx(BaseManager.getInstance().getContext(), menuTextTextSize2));
//        }
//        if (leftImg != -1) {
//            menuLogoIv.setVisibility(View.VISIBLE);
//            menuLogoIv.setImageResource(leftImg);
//        }else{
//            menuLogoIv.setVisibility(View.GONE);
//        }
//        if(!isrightImg){
//            jtIv.setVisibility(View.INVISIBLE);
//        }
//        if (rightImg != -1) {
//            jtIv.setImageResource(rightImg);
//        }
//        if(rightheadImg != -1){
//            head.setImageResource(rightheadImg);
//            head.setVisibility(View.VISIBLE);
//        }
//
//        int left_paddingleft = a.getInteger(R.styleable.HorizontalCommonMenu_left_paddingleft, -1);
//        int right_marginRight = a.getInteger(R.styleable.HorizontalCommonMenu_right_marginRight, -1);
//        if (left_paddingleft != -1) {
//            menuTv.setPadding(Utils.dipToPx(BaseManager.getInstance().getContext(), left_paddingleft), 0, 0, 0);
//        }
//
//        if (right_marginRight != -1) {
//            LayoutParams rlp = (LayoutParams) jtIv.getLayoutParams();
//            rlp.rightMargin = right_marginRight;
//        }
//
//
////        ColorStateList menuTextColor = a.getColorStateList(R.styleable.HorizontalCommonMenu_menuTextColor);
//        int menuTextColor = a.getColor(R.styleable.HorizontalCommonMenu_menuTextColor, -1);
//        if (menuTextColor != -1) {
//            menuTv.setTextColor(menuTextColor);
//        }
//
//        int menuTextColor2 = a.getColor(R.styleable.HorizontalCommonMenu_menuTextColor2, -1);
//        if (menuTextColor2 != -1) {
//            menuTv2.setTextColor(menuTextColor2);
//        }
//
//        int rightTextColor = a.getColor(R.styleable.HorizontalCommonMenu_rightTextColor, -1);
//        if (rightTextColor != -1) {
//            msgTv.setTextColor(rightTextColor);
//        }
//
//        menuTv.setText(menuText);
//        menuTv2.setText(menuText2);
//        msgTv.setText(rightText);
//
//        a.recycle();
//    }
//
//    public ImageView getheadID(){
//        return head;
//    }
//
//    public void setRightImge(int rightImg) {
//        if(rightImg==-1){
//            jtIv.setVisibility(GONE);
//        }else{
//            jtIv.setVisibility(VISIBLE);
//            jtIv.setImageResource(rightImg);
//        }
//    }
//
//    public void setMenuLogo(String path){
//        menuLogoIv.setVisibility(View.VISIBLE);
//        ImageLoaderHelper.loadImageByGlide(menuLogoIv,path,-1,null);
//    }
//
//    public void setMenuText(String menuText) {
//        this.menuText = menuText;
//        menuTv.setText(menuText);
//    }
//
//    public void setMenuText2(String menuText2) {
//        this.menuText2 = menuText2;
//        menuTv2.setText(menuText2);
//    }
//
//    public void appendMenuText(String appendText, int color){
//        this.menuText += appendText;
//        ViewUtil.Companion.appendSpan(menuTv, new TextSpanCreater.Builder(appendText).setColor(color).build().create());
//    }
//
//    public void setMenuText(Spanned menuText) {
//        menuTv.setText(menuText);
//    }
//
//    public void setRightText(String rightText, int color) {
//
//        if(rightText!=null && rightText.length()>15){
//            rightText = rightText.substring(0,14)+"..";
//        }
//        this.rightText = rightText;
//        msgTv.setText(rightText);
//        if (color != -1) {
//            msgTv.setTextColor(color);
//        }
//    }
//
//    public void setJtHide(){
//        jtLL.setVisibility(INVISIBLE);
//    }
//
//}
