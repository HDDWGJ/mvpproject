package com.sowell.mvpbase.view.widget.span

import android.R
import android.text.Spannable
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.style.*
import android.view.View
import android.widget.TextView
import com.maoti.lib.BaseManager
import com.sowell.mvpbase.mvputils.DisplayUtil

/**
 *
 * span构造器
 */
class TextSpanCreater(builder: Builder) {

    var builder: Builder? = builder

    fun create(): Spannable {
        val ss = SpannableString(builder?.spanText)
        if (builder?.listener != null) {
            ss.setSpan(MyClickableSpan(builder?.listener),  // Span接口用于实现对文本的修饰的具体内容
                    0,  // 修饰的起始位置
                    builder?.spanText!!.length,  // 修饰的结束位置
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
        if (builder?.color != -1) {
            ss.setSpan(ForegroundColorSpan(builder?.color
                    ?: -1), 0, builder?.spanText!!.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
        if (builder?.isunderline == true) {
            ss.setSpan(UnderlineSpan(), 0, builder?.spanText!!.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }

        // dip，如果为true，表示前面的字体大小单位为dip
        if (builder?.textsize != -1) {
//                ss.setSpan(AbsoluteSizeSpan(textsize, true), 0, spanText!!.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE) // 第二个参数boolean
            ss.setSpan(AbsoluteSizeSpan(DisplayUtil.sptopx(BaseManager.getInstance().getContext(), builder?.textsize!!.toFloat())), 0, builder?.spanText!!.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)//换成sp单位
        }

        // Typeface.NORMAL正常
        // Typeface.ITALIC斜体
        // Typeface.BOLD_ITALIC粗斜体
        if (builder?.style != -1) {
            ss.setSpan(StyleSpan(builder?.style
                    ?: -1), 0, builder?.spanText!!.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        } // Typeface.BOLD粗体

        return ss
    }

    class Builder(var spanText: String?) {

        var color: Int? = -1
        var textsize: Int? = -1
        var style: Int? = -1
        var isunderline: Boolean? = false
        var listener: View.OnClickListener? = null

        fun setColor(color: Int): Builder{
            this.color = color
            return this
        }

        fun setTextSize(textsize: Int): Builder{
            this.textsize = textsize
            return this
        }

        fun setStyle(style: Int): Builder{
            this.style = style
            return this
        }

        fun setIsUnderline(isunderline: Boolean): Builder{
            this.isunderline = isunderline
            return this
        }

        fun setListener(listener: View.OnClickListener): Builder{
            this.listener = listener
            return this
        }

        fun build(): TextSpanCreater {
            return TextSpanCreater(this)
        }
    }

    /**
     * 用于更改文字点击的事件和效果
     */
    private class MyClickableSpan(private val mOnClickListener: View.OnClickListener?) : ClickableSpan() {
        override fun onClick(widget: View) {
            mOnClickListener?.onClick(widget)
            if (widget != null && widget is TextView) {
                widget.highlightColor = widget.getContext().resources.getColor(R.color.transparent) //方法重新设置文字背景为透明色
            }
        }

        override fun updateDrawState(ds: TextPaint) {
//            super.updateDrawState(ds);
//            //设置文本的颜色
//            ds.setColor(Color.RED);
//            //超链接形式的下划线，false 表示不显示下划线，true表示显示下划线
//            ds.setUnderlineText(false);
        }
    }

    companion object {
        private const val TAG = "TextSpanCreater"
    }

}