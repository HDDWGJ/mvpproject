package com.sowell.mvpbase.view.widget.span

import android.view.View
import android.widget.TextView
import com.sowell.mvpbase.mvputils.ViewUtil

import java.lang.ref.WeakReference

/**
 *
 * textView多样式
 */
class MutiStyleHelper(builder: Builder) {

    var builder: Builder? = builder

    fun create() {
        var tv = builder?.targetReference?.get()
        if (tv != null){
            ViewUtil.setTextViewStyleAndOnClick(tv.context,tv,builder?.spanTexts,builder?.colors,builder?.textsizes,builder?.styles,builder?.isunderlines,builder?.listeners,null)
        }
    }

    class Builder(targetTv: TextView?, var spanTexts: Array<String>?) {
        var targetReference: WeakReference<TextView?> = WeakReference(targetTv)
        var colors: IntArray? = IntArray(spanTexts?.size?:0)
        var textsizes: IntArray? = IntArray(spanTexts?.size?:0)
        var styles: IntArray? = IntArray(spanTexts?.size?:0)
        var isunderlines: BooleanArray? = BooleanArray(spanTexts?.size?:0)
        var listeners: Array<View.OnClickListener?>? = arrayOf()

        fun setmColors(colors: IntArray): Builder {
            this.colors = colors
            return this
        }

        fun setmTextSizes(textsizes: IntArray): Builder {
            this.textsizes = textsizes
            return this
        }

        fun setmStyles(styles: IntArray): Builder {
            this.styles = styles
            return this
        }

        fun setmIsUnderlines(isunderlines: BooleanArray): Builder {
            this.isunderlines = isunderlines
            return this
        }

        fun setmListeners(listeners: Array<View.OnClickListener?>): Builder {
            this.listeners = listeners
            return this
        }

        fun build(): MutiStyleHelper {
            return MutiStyleHelper(this)
        }
    }

    companion object {
        private const val TAG = "MutiStyleHelper"
    }

}