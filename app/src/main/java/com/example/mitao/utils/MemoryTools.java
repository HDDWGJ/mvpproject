package com.example.mitao.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.text.format.Formatter;
import android.util.Log;

public class MemoryTools {
    public static String getAvailMemory(Context context) {// 获取android当前可用内存大小

        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        am.getMemoryInfo(mi);
        return Formatter.formatFileSize(context, mi.availMem);// 将获取的内存大小规格化
    }

    private static long  totalMemory =0;

    public static long getTotalMemory(Context context) {// 获取android当前可用内存大小
        if(totalMemory>0){
            return totalMemory;
        }
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        am.getMemoryInfo(mi);
        totalMemory=mi.totalMem/1024/1024/1024;
        return totalMemory;
    }

    public static void getTotalCpu(Context context) {// 获取android当前可用内存大小

        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        am.getMemoryInfo(mi);
//        totalMemory=mi.totalMem/1024/1024/1024;
//        return totalMemory;
    }

    public static boolean isLowLevelMachine(Context context){
//        getTotalCpu(context);
        //内存少于5g,认为是低端机
        if(getTotalMemory(context)<5){
            Log.d("机型", "isLowLevelMachine: 低端机型");
            return true;
        }
        Log.d("机型", "isLowLevelMachine: 高端机型");
        return false;
    }

}
