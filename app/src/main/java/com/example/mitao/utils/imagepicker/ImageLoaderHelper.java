//package com.example.mitao.utils.imagepicker;
//
//import android.app.Activity;
//import android.content.Context;
//import android.view.View;
//import android.widget.ImageView;
//
//import com.example.mitao.BaseApplication;
//
//
//public class ImageLoaderHelper {
//
//	/**
//	 *
//	 * 2018年5月18日
//	 * 作者：csj
//	 * 方法描述：加载图片，不需要缓存
//	 * @param iv
//	 * @param path
//	 * @param defaultImage_resId
//	 */
//
//	private final static int icon_defual_news = ResourcesUtil.getResource(BaseApplication.getApplication(), R.mipmap.icon_defual_news);
//
//	public static void loadImageByGlideNotCache(ImageView iv, String path, int defaultImage_resId, RequestListener listener){
//		if(iv==null){
//			return;
//		}
//		if (iv.getContext() instanceof Activity && ((Activity)iv.getContext()).isFinishing()){
//			return;
//		}
//		int defaultImage = ResourcesUtil.getResource(iv.getContext(),defaultImage_resId);
//		GlideApp
//	    .with(getContext(iv))
//	    .load(path)
////	    .centerCrop()
//	    .diskCacheStrategy(DiskCacheStrategy.NONE )//禁用磁盘缓存
//	    .skipMemoryCache( true )//跳过内存缓存
//	    .placeholder(defaultImage_resId!=-1?defaultImage: R.drawable.ic_default_image)
//		.listener(listener)
//	    .into(iv);
//	}
//
//	public static void loadImageByGlideNotCacheAsBitmap(ImageView iv, String path, int defaultImage_resId, RequestListener listener){
//		if(iv==null){
//			return;
//		}
//		if (iv.getContext() instanceof Activity && ((Activity)iv.getContext()).isFinishing()){
//			return;
//		}
//		int defaultImage = ResourcesUtil.getResource(iv.getContext(),defaultImage_resId);
//		GlideApp
//				.with(getContext(iv))
//				.asBitmap()
//				.load(path)
////	    .centerCrop()
//				.diskCacheStrategy(DiskCacheStrategy.NONE )//禁用磁盘缓存
//				.skipMemoryCache( true )//跳过内存缓存
//				.placeholder(defaultImage_resId!=-1?defaultImage: R.drawable.ic_default_image)
////				.crossFade()
//				.listener(listener)
//				.into(iv);
//	}
//
//
//	public static void loadImageByGlide(ImageView iv, String path, int defaultImage_resId, boolean getsmall){
//		if(iv==null){
//			return;
//		}
//		if (iv.getContext() instanceof Activity && ((Activity)iv.getContext()).isFinishing()){
//			return;
//		}
//		int defaultImage = ResourcesUtil.getResource(iv.getContext(),defaultImage_resId);
//
//		GlideApp
//	    .with(getContext(iv))
//	    .load(path)
////	    .centerCrop()
//	    .placeholder(defaultImage_resId!=-1?defaultImage: R.drawable.ic_default_image)
////				.listener(new RequestListener<String, GlideDrawable>() {
////					@Override
////					public boolean onException(Exception e, String s, Target<GlideDrawable> target, boolean b) {
////						return false;
////					}
////
////					@Override
////					public boolean onResourceReady(GlideDrawable glideDrawable, String s, Target<GlideDrawable> target, boolean b, boolean b1) {
////						return false;
////					}
////				})
//	    .into(iv);
//	}
//
//	public static Context getContext(View v){
//		return (v != null && v.getContext() != null)?v.getContext():BaseViewManager.getInstance().getApplication();
//	}
//
//	public static void loadImageByGlide(ImageView iv, String path, int defaultImage_resId, RequestListener listener){
//		if(iv==null){
//			return;
//		}
//		if (iv.getContext() instanceof Activity && ((Activity)iv.getContext()).isFinishing()){
//			return;
//		}
//		int defaultImage = ResourcesUtil.getResource(iv.getContext(),defaultImage_resId);
//		GlideApp
//				.with(getContext(iv))
//				.asDrawable()
//				.load(path)
//				.diskCacheStrategy(DiskCacheStrategy.ALL)
//				.placeholder(defaultImage_resId!=-1?defaultImage :R.drawable.ic_default_image)
//				.error(defaultImage_resId!=-1?defaultImage:R.drawable.ic_default_image)
//				.listener(listener)
//				.into(iv);
//	}
//
//	public static void loadImageByGlide(ImageView iv, String path, int defaultImage_resId, RequestOptions options, RequestListener listener){
//		if(iv==null){
//			return;
//		}
//		if (iv.getContext() instanceof Activity && ((Activity)iv.getContext()).isFinishing()){
//			return;
//		}
//		int defaultImage = ResourcesUtil.getResource(iv.getContext(),defaultImage_resId);
//		GlideApp
//				.with(getContext(iv))
//				.load(path)
//				.diskCacheStrategy(DiskCacheStrategy.ALL)
//				.placeholder(defaultImage_resId!=-1?defaultImage:icon_defual_news)
//				.error(defaultImage_resId!=-1?defaultImage:icon_defual_news)
//				.listener(listener)
//				.apply(options)
//				.into(iv);
//	}
//
//	public static void loadImageByGlideAsBitmap(ImageView iv, String path, int defaultImage_resId, RequestListener listener){
//		if(iv==null){
//			return;
//		}
//		if (iv.getContext() instanceof Activity && ((Activity)iv.getContext()).isFinishing()){
//			return;
//		}
//		int defaultImage = ResourcesUtil.getResource(iv.getContext(),defaultImage_resId);
//
//		GlideApp
//				.with(getContext(iv))
//				.asBitmap()//在Glide 3中的语法是先load()再asBitmap()的，而在Glide 4中是先asBitmap()再load()
//				.load(path)
//
////	    .centerCrop()
//				.placeholder(defaultImage_resId!=-1?defaultImage:R.drawable.ic_default_image)
//				.error(R.drawable.ic_default_image)
//				.listener(listener)
////				.listener(new RequestListener<String, GlideDrawable>() {
////					@Override
////					public boolean onException(Exception e, String s, Target<GlideDrawable> target, boolean b) {
////						return false;
////					}
////
////					@Override
////					public boolean onResourceReady(GlideDrawable glideDrawable, String s, Target<GlideDrawable> target, boolean b, boolean b1) {
////						return false;
////					}
////				})
//				.into(iv);
//	}
//
//	public static void loadImageByGlideAsBitmap(ImageView iv, String path, int defaultImage_resId, RequestOptions options, RequestListener listener){
//		if(iv==null){
//			return;
//		}
//		if (iv.getContext() instanceof Activity && ((Activity)iv.getContext()).isFinishing()){
//			return;
//		}
//		int defaultImage = ResourcesUtil.getResource(iv.getContext(),defaultImage_resId);
//
//		GlideApp
//				.with(getContext(iv))
//				.asBitmap()//在Glide 3中的语法是先load()再asBitmap()的，而在Glide 4中是先asBitmap()再load()
//				.load(path)
//				.placeholder(defaultImage_resId!=-1?defaultImage:R.drawable.ic_default_image)
//				.error(R.drawable.ic_default_image)
//				.listener(listener)
//				.apply(options)
//				.into(iv);
//	}
//
//	/**
//	 * 仅加载图片，不做显示
//	 * @param path
//	 * @param target
//	 */
//	public static void loadImageSimpleTargetOnly(String path, SimpleTarget target) {
//		GlideApp
//				.with(BaseViewManager.getInstance().getApplication()) // could be an issue!
//				.asBitmap()   //制转换Bitmap
//				.load(path)
//				.into(target);
//	}
//
//	public static void loadImageSimpleTarget(String path, int defaultRes, SimpleTarget target) {
//		int defaultImage = ResourcesUtil.getResource(BaseViewManager.getInstance().getApplication(),defaultRes);
//		GlideApp
//				.with(BaseViewManager.getInstance().getApplication()) // could be an issue!
//				.asBitmap()   //制转换Bitmap
//				.load(path)
//				.placeholder(defaultRes == -1?R.drawable.ic_default_image:defaultImage)
//				.fallback(defaultRes == -1?R.drawable.ic_default_image:defaultImage)
//				.error(defaultRes == -1?R.drawable.ic_default_image:defaultImage)
//				.diskCacheStrategy(DiskCacheStrategy.ALL)
//				.into(target);
//	}
//
//	public static void loadGifFromSrc(ImageView iv, int src, int defaultRes){
//		if (iv.getContext() instanceof Activity && ((Activity)iv.getContext()).isFinishing()){
//			return;
//		}
//		int defaultImage = ResourcesUtil.getResource(BaseViewManager.getInstance().getApplication(),defaultRes);
//		GlideApp
//				.with(getContext(iv)) // could be an issue!
//				.asGif()   //
//				.load(src)
//				.placeholder(defaultRes == -1?R.drawable.ic_default_image:defaultImage)
//				.fallback(defaultRes == -1?R.drawable.ic_default_image:defaultImage)
//				.error(defaultRes == -1?R.drawable.ic_default_image:defaultImage)
//				.diskCacheStrategy(DiskCacheStrategy.ALL)
//				.into(iv);
//	}
//
//}
