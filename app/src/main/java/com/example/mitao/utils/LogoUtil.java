package com.example.mitao.utils;

import android.content.ComponentName;
import android.content.pm.PackageManager;
import android.util.Log;

import com.example.mitao.BaseApplication;

public class LogoUtil {

    private final static String[] componentNames = new String[]
            {
                    "LauncherActivity",
                    "icon_default",
                    "style_j01",
                    "style_j02",
                    "style_j03",
                    "style_j04",
                    "style_s01",
                    "style_s02",
                    "style_s03",
                    "style_s04",
                    "style_s05",
                    "style_s06",
                    "style_s07",
                    "style_s08"
            };

    private final static String PACKAGE = "com.sowell.maoti.";

    private static boolean isSpecialLogo(String logo) {
        for (String s : componentNames) {
            if (s.equals(logo)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 启用组件  动态更换桌面logo*
     *
     * @param componentName 重要方法
     */
    public static void enableComponent(String componentName) {
        try {
            String cnName = componentName;
            if (!isSpecialLogo(cnName)) {
                cnName = "icon_default";
            }
            ComponentName cn = new ComponentName(BaseApplication.getContext(), PACKAGE + cnName);
            PackageManager pm = BaseApplication.getContext().getPackageManager();
            int state = pm.getComponentEnabledSetting(cn);
            for (String s : componentNames) {
                if (!s.equals(cnName)) {
                    disableComponent(s);
                }
            }
            if (PackageManager.COMPONENT_ENABLED_STATE_ENABLED == state) {
                //已经启用
                return;
            }

            Log.d("hdyip", "enableComponent: "+componentName+"被启动");
            pm.setComponentEnabledSetting(cn,
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                    PackageManager.DONT_KILL_APP);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 禁用组件  动态更换桌面logo*
     *
     * @param componentName 重要方法
     */
    public static void disableComponent(String componentName) {
        try {
            ComponentName cn = new ComponentName(BaseApplication.getContext(), PACKAGE + componentName);
            PackageManager pm = BaseApplication.getContext().getPackageManager();
            int state = pm.getComponentEnabledSetting(cn);

            if (PackageManager.COMPONENT_ENABLED_STATE_DISABLED == state) {
                //已经禁用
                return;
            }
            Log.d("hdyip", "disableComponent: "+componentName+"被关闭了");
            pm.setComponentEnabledSetting(cn,
                    PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                    PackageManager.DONT_KILL_APP);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
