package com.example.mitao.base;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;

import android.widget.FrameLayout;

import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;


import com.example.mitao.R;
import com.example.mitao.utils.DisplayUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kenny on 2020/5/23.
 */
public abstract class BaseDialogView extends FrameLayout {

    private static final String TAG = "BaseDialogFragment";

    public final String TOP = "TOP";
    public final String BOTTM = "BOTTM";
    public final String CENTER = "CENTER";
    public final String RIGHT = "RIGHT";

    /**
     * 数据中转
     */
    protected Object tag;
    private Map<String, Object> mapTags = new HashMap<>();
    private View contentV;
    private boolean cancelable = true;

    public BaseDialogView(@NonNull Context context) {
        super(context);
        init();
    }

    public void showView(){

    }

    public void setData(Object tag) {
        this.tag = tag;
    }

    public void setData(String key, Object tag) {
        this.mapTags.put(key, tag);
    }

    public Object getData() {
        return tag;
    }

    public Object getData(String key) {
        return this.mapTags.get(key);
    }

    public View getView(){
        return contentV;
    }

    public void setCancelable(boolean cancelable){
        this.cancelable = cancelable;
    }


    @SuppressLint("ResourceAsColor")
    @Nullable
    public void init() {
        int viewId = getContentViewResId();
        if (viewId > 0) {
            contentV = inflate(getContext(), viewId, this);
        }

        if (getCloseBtnId() > 0) {
            View view = findViewById(getCloseBtnId());
            if (view != null){
                view.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setVisibility(GONE);
                    }
                });
            }
        }

        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cancelable){
                    setVisibility(GONE);
                }
            }
        });

        setBackgroundResource(R.mipmap.bg_dialog_v);
        if (contentV != null){
            LayoutParams lp = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            if (TOP.equals(getLocation())) {
                lp.gravity = Gravity.CENTER_HORIZONTAL | Gravity.TOP;
            } else if (CENTER.equals(getLocation())) {
                lp.gravity = Gravity.CENTER;
            } else if (RIGHT.equals(getLocation())) {
                lp.gravity = Gravity.CENTER_VERTICAL | Gravity.RIGHT;
            } else {
                lp.gravity = Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM;
            }
            int dp20 = (int) DisplayUtil.diptopx(getContext(), 20F);
            lp.setMargins(dp20,0,dp20,0);
            contentV.setLayoutParams(lp);
            resize();
        }
    }

    public void show(FragmentActivity activity) {
        setVisibility(VISIBLE);
        initData(contentV);
    }


    public void show(FragmentActivity activity, RelativeLayout vg, int bottomMargin){
        View v = vg.findViewById(R.id.forecast_dialog_id);
        if (v != null){
            vg.removeView(v);
        }
        setId(R.id.forecast_dialog_id);
        RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
        rlp.bottomMargin = bottomMargin;
        vg.addView(this, rlp);
        bringToFront();
        show(activity);
    }

    public void resize() {

    }



//    public void getInstanceState(Bundle savedInstanceState){}

    public abstract View initData(View view);

    public abstract int getContentViewResId();

    public abstract int getCloseBtnId();

    public abstract String getLocation();
}
