package com.example.mitao;

import android.app.Application;
import android.content.Context;

import com.example.mitao.utils.SPfUtil;
import com.example.mitao.utils.key.SPFKey;


public class BaseApplication extends Application{

    private static Context context;

    private static Application application;

    @Override
    public void onCreate() {
        super.onCreate();
        context=this;
    }


    public static Context getContext(){
        return context;
    }

    public static Application getApplication(){
        return application;
    }

    public static boolean isLite(){
        return false;
    }
    public static Boolean isSingIN() {
        return SPfUtil.getInstance().getBoolean(SPFKey.IsSingIN);
    }


}
