package com.example.mitao.model.service;


import com.example.mitao.net.Constant;
import com.example.mitao.net.ResponseResult;
import com.example.mitao.ui.home.activitytest.bean.TestBean;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiCourseService {


    @POST("user/login")
    Observable<ResponseResult<Object>> login(@Body RequestBody requestBody);

    /**
     * 获取赛程时间和第几轮
     */
    @POST("user/login")
    Observable<ResponseResult<List<TestBean>>> getTestList(@Body RequestBody requestBody);

    /**
     * 获取赛程时间和第几轮
     */
    @POST(Constant.EditionPath+"/test")
    Observable<ResponseResult<TestBean>> getTest(@Body RequestBody requestBody);

}
