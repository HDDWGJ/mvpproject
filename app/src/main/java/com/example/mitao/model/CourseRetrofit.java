package com.example.mitao.model;

import com.example.mitao.model.service.ApiCourseService;
import com.example.mitao.net.RetrofitUtil;
import com.example.mitao.net.interceptor.DefaultObserver;
import com.example.mitao.ui.home.activitytest.bean.TestBean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class CourseRetrofit extends RetrofitUtil {
    /**
     * 集合返回
     * @param observer
     * @param json
     */
    public static void getCourse(DefaultObserver<List<TestBean>> observer, String json) {
        Map<String, Object> map = new HashMap<>();
        map.put("channel_id", "");
        map.put("page", 1);
        map.put("page_size", "20");
        getGsonRetrofitNoCache()
                .create(ApiCourseService.class)
                .getTestList(toJsonBody(map))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }


    /**
     * 数据bean返回
     * @param observer
     * @param json
     */
    public static void getCourses(DefaultObserver<TestBean> observer, String json) {
        getGsonRetrofitNoCache()
                .create(ApiCourseService.class)
                .getTest(getJsonBody(json))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    public static void Login(DefaultObserver<Object> observer, String json) {
        Map<String, Object> map = new HashMap<>();
        map.put("code", "");
        map.put("mobile", "");
        map.put("password", "");
        getGsonRetrofitNoCache()
                .create(ApiCourseService.class)
                .login(toJsonBody(map))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

}
